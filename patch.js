const { app, session, BrowserWindow } = require('electron')
const fs = require('fs')

/** @type {BrowserWindow} */
var win;

class PatchHelper {
    static getSessionID(trackId, sessid) {
        return new Promise(async (res, rej) => {

            if (win) {
                await new Promise((res1,rej1) => {
                    setTimeout(() => {
                        if (!win) res1()
                    }, 500);
                })
                res()
                return
            }
            win = true

            await app.whenReady()

            if (sessid) await session.defaultSession.cookies.set({
                domain: "free-mp3-download.net",
                name: "PHPSESSID",
                url: "https://free-mp3-download.net/",
                value: sessid
            })

            win = new BrowserWindow({ width: 350, height: 580, resizable: false, title: "Loading...", 'autoHideMenuBar': true })


            win.webContents.insertCSS(fs.readFileSync(__dirname + '/css/captcha.css').toString())
            await win.loadURL(`https://free-mp3-download.net/download.php?id=${trackId || '1467490342'}`)
            win.webContents.executeJavaScript("$('.dl.btn').text('Continue')")
            win.webContents.insertCSS(fs.readFileSync(__dirname + '/css/captcha-post.css').toString())

            win.setTitle("CAPTCHA 😔")

            session.defaultSession.webRequest.onBeforeRequest({ urls: ['https://free-mp3-download.net/*'] }, async (details, cb) => {
                if (details.url.includes('/dl.php?')) {
                    console.log(details)
                    cb({ cancel: true })
                    win.close()
                    win = null
                    let cookie = (await session.defaultSession.cookies.get({
                        domain: "free-mp3-download.net",
                        name: "PHPSESSID"
                    }))[0]
                    let params = new URLSearchParams(details.url.split("?")[1])
                    if (!params.has('h')) { 
                        rej("No h value given")
                        return;
                    }
                    res({
                        sessid: cookie.value,
                        h: params.get('h')
                    })
                } else {
                    cb({ cancel: false })
                }

            })
        })
    }
}

exports.PatchHelper = PatchHelper