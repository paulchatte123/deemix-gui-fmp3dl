// @ts-nocheck
// @ts-expect-error
/* eslint-disable */
import deemix from 'deemix'
import { mp3dlSession } from './session'
import fs from 'fs'
// const fs = require('fs')
const { tmpdir } = require('os')
const { exec } = require('child_process')
const { Track } = require('deemix/deemix/types/Track.js')
const { StaticPicture } = require('deemix/deemix/types/Picture.js')
const { streamTrack, generateCryptedStreamURL } = require('deemix/deemix/decryption.js')
const { tagID3, tagID3v1, tagFLAC } = require('deemix/deemix/tagger.js')
const { USER_AGENT_HEADER, pipeline, shellEscape } = require('deemix/deemix/utils/index.js')
const { DEFAULTS, OverwriteOption } = require('deemix/deemix/settings.js')
const {
	generatePath,
	generateAlbumName,
	generateArtistName,
	generateDownloadObjectName
} = require('deemix/deemix/utils/pathtemplates.js')
const {
	PreferredBitrateNotFound,
	TrackNot360,
	DownloadFailed,
	ErrorMessages,
	DownloadCanceled
} = require('deemix/deemix/errors.js')
const { TrackFormats } = require('deezer-js')
const { WrongLicense, WrongGeolocation } = require('deezer-js').errors
const got = require('got')
const { queue, each } = require('async')

console.log('Patch Implemented')

const extensions = {
	[TrackFormats.FLAC]: '.flac',
	[TrackFormats.LOCAL]: '.mp3',
	[TrackFormats.MP3_320]: '.mp3',
	[TrackFormats.MP3_128]: '.mp3',
	[TrackFormats.DEFAULT]: '.mp3',
	[TrackFormats.MP4_RA3]: '.mp4',
	[TrackFormats.MP4_RA2]: '.mp4',
	[TrackFormats.MP4_RA1]: '.mp4'
}

const formatsName = {
	[TrackFormats.FLAC]: 'FLAC',
	[TrackFormats.LOCAL]: 'MP3_MISC',
	[TrackFormats.MP3_320]: 'MP3_320',
	[TrackFormats.MP3_128]: 'MP3_128',
	[TrackFormats.DEFAULT]: 'MP3_MISC',
	[TrackFormats.MP4_RA3]: 'MP4_RA3',
	[TrackFormats.MP4_RA2]: 'MP4_RA2',
	[TrackFormats.MP4_RA1]: 'MP4_RA1'
}

const TEMPDIR = tmpdir() + `/deemix-imgs`

export class PatchedDownloader extends deemix.downloader.Downloader {
	async download(extraData: any, track: any) {
		console.log('Using the overridden download function!', this)

		const returnData = {}
		const { trackAPI_gw, trackAPI, albumAPI, playlistAPI } = extraData
		trackAPI_gw.SIZE = this.downloadObject.size
		if (this.downloadObject.isCanceled) throw new DownloadCanceled()
		if (trackAPI_gw.SNG_ID == '0') throw new DownloadFailed('notOnDeezer')

		let itemData = {
			id: trackAPI_gw.SNG_ID,
			title: trackAPI_gw.SNG_TITLE.trim(),
			artist: trackAPI_gw.ART_NAME
		}

		// Generate track object
		if (!track) {
			track = new Track()
			this.log(itemData, 'getTags')
			try {
				await track.parseData(
					this.dz,
					trackAPI_gw.SNG_ID,
					trackAPI_gw,
					trackAPI,
					null, // albumAPI_gw
					albumAPI,
					playlistAPI
				)
			} catch (e) {
				if (e.name === 'AlbumDoesntExists') {
					throw new DownloadFailed('albumDoesntExists')
				}
				if (e.name === 'MD5NotFound') {
					throw new DownloadFailed('notLoggedIn')
				}
				console.trace(e)
				throw e
			}
			this.log(itemData, 'gotTags')
		}
		if (this.downloadObject.isCanceled) throw new DownloadCanceled()

		itemData = {
			id: track.id,
			title: track.title,
			artist: track.mainArtist.name
		}

		// Check if the track is encoded
		if (track.MD5 === '') throw new DownloadFailed('notEncoded', track)

		console.log(track)

		// Check the target bitrate
		this.log(itemData, 'getBitrate')
		let selectedFormat
		try {
			selectedFormat = await deemix.downloader.getPreferredBitrate(
				this.dz,
				track,
				this.bitrate,
				this.settings.fallbackBitrate,
				this.downloadObject.uuid,
				this.listener
			)
		} catch (e) {
			if (e.name === 'WrongLicense') {
				throw new DownloadFailed('wrongLicense')
			}
			if (e.name === 'WrongGeolocation') {
				throw new DownloadFailed('wrongGeolocation')
			}
			if (e.name === 'PreferredBitrateNotFound') {
				throw new DownloadFailed('wrongBitrate', track)
			}
			if (e.name === 'TrackNot360') {
				throw new DownloadFailed('no360RA')
			}
			console.trace(e)
			throw e
		}
		track.bitrate = selectedFormat
		track.album.bitrate = selectedFormat
		this.log(itemData, 'gotBitrate')

		// Apply Settings
		track.applySettings(this.settings)

		// Generate filename and filepath from metadata
		const { filename, filepath, artistPath, coverPath, extrasPath } = generatePath(
			track,
			this.downloadObject,
			this.settings
		)
		if (this.downloadObject.isCanceled) throw new DownloadCanceled()

		// Make sure the filepath exsists
		fs.mkdirSync(filepath, { recursive: true })
		console.log(this.bitrate, typeof this.bitrate)
		var extension = (() => {
			switch (this.bitrate) {
				case '-10': return '.flac'
				case '-5': return '.mp3'
				default: return extensions[track.bitrate]
			}
		})()
		var writepath = `${filepath}/${filename}${extension}`

		// Save extrasPath
		if (extrasPath && !this.downloadObject.extrasPath) {
			this.downloadObject.extrasPath = extrasPath
		}

		// Generate covers URLs
		let embeddedImageFormat = `jpg-${this.settings.jpegImageQuality}`
		if (this.settings.embeddedArtworkPNG) embeddedImageFormat = 'png'

		track.album.embeddedCoverURL = track.album.pic.getURL(this.settings.embeddedArtworkSize, embeddedImageFormat)
		let ext = track.album.embeddedCoverURL.slice(-4)
		if (ext.charAt(0) != '.') ext = '.jpg'
		track.album.embeddedCoverPath = `${TEMPDIR}/${
			track.album.isPlaylist ? 'pl' + track.playlist.id : 'alb' + track.album.id
		}_${this.settings.embeddedArtworkSize}${ext}`

		// Download and cache the coverart
		this.log(itemData, 'getAlbumArt')
		if (!this.coverQueue[track.album.embeddedCoverPath])
			this.coverQueue[track.album.embeddedCoverPath] = deemix.downloader.downloadImage(
				track.album.embeddedCoverURL,
				track.album.embeddedCoverPath
			)
		track.album.embeddedCoverPath = await this.coverQueue[track.album.embeddedCoverPath]
		if (this.coverQueue[track.album.embeddedCoverPath]) delete this.coverQueue[track.album.embeddedCoverPath]
		this.log(itemData, 'gotAlbumArt')

		// Save local album art
		if (coverPath) {
			returnData.albumURLs = []
			this.settings.localArtworkFormat.split(',').forEach(picFormat => {
				if (['png', 'jpg'].includes(picFormat)) {
					let extendedFormat = picFormat
					if (extendedFormat == 'jpg') extendedFormat += `-${this.settings.jpegImageQuality}`
					const url = track.album.pic.getURL(this.settings.localArtworkSize, extendedFormat)
					// Skip non deezer pictures at the wrong format
					if (track.album.pic instanceof StaticPicture && picFormat != 'jpg') return
					returnData.albumURLs.push({ url, ext: picFormat })
				}
			})
			returnData.albumPath = coverPath
			returnData.albumFilename = generateAlbumName(
				this.settings.coverImageTemplate,
				track.album,
				this.settings,
				track.playlist
			)
		}

		// Save artist art
		if (artistPath) {
			returnData.artistURLs = []
			this.settings.localArtworkFormat.split(',').forEach(picFormat => {
				// Deezer doesn't support png artist images
				if (picFormat === 'jpg') {
					const extendedFormat = `${picFormat}-${this.settings.jpegImageQuality}`
					const url = track.album.mainArtist.pic.getURL(this.settings.localArtworkSize, extendedFormat)
					// Skip non deezer pictures at the wrong format
					if (track.album.mainArtist.pic.md5 == '') return
					returnData.artistURLs.push({ url, ext: picFormat })
				}
			})
			returnData.artistPath = artistPath
			returnData.artistFilename = generateArtistName(
				this.settings.artistImageTemplate,
				track.album.mainArtist,
				this.settings,
				track.album.rootArtist
			)
		}

		// Save playlist art
		if (track.playlist) {
			if (this.playlistURLs.length == 0) {
				this.settings.localArtworkFormat.split(',').forEach(picFormat => {
					if (['png', 'jpg'].includes(picFormat)) {
						let extendedFormat = picFormat
						if (extendedFormat == 'jpg') extendedFormat += `-${this.settings.jpegImageQuality}`
						const url = track.playlist.pic.getURL(this.settings.localArtworkSize, extendedFormat)
						// Skip non deezer pictures at the wrong format
						if (track.playlist.pic instanceof StaticPicture && picFormat != 'jpg') return
						this.playlistURLs.push({ url, ext: picFormat })
					}
				})
			}
			if (!this.playlistCovername) {
				track.playlist.bitrate = track.bitrate
				track.playlist.dateString = track.playlist.date.format(this.settings.dateFormat)
				this.playlistCovername = generateAlbumName(
					this.settings.coverImageTemplate,
					track.playlist,
					this.settings,
					track.playlist
				)
			}
		}

		// Save lyrics in lrc file
		if (this.settings.syncedLyrics && track.lyrics.sync) {
			if (
				!fs.existsSync(`${filepath}/${filename}.lrc`) ||
				[OverwriteOption.OVERWRITE, OverwriteOption.ONLY_TAGS].includes(this.settings.overwriteFile)
			)
				fs.writeFileSync(`${filepath}/${filename}.lrc`, track.lyrics.sync)
		}

		// Check for overwrite settings
		let trackAlreadyDownloaded = fs.existsSync(writepath)

		// Don't overwrite and don't mind extension
		if (!trackAlreadyDownloaded && this.settings.overwriteFile == OverwriteOption.DONT_CHECK_EXT) {
			const extensions = ['.mp3', '.flac', '.opus', '.m4a']
			const baseFilename = `${filepath}/${filename}`
			for (let i = 0; i < extensions.length; i++) {
				const ext = extensions[i]
				trackAlreadyDownloaded = fs.existsSync(baseFilename + ext)
				if (trackAlreadyDownloaded) break
			}
		}

		// Don't overwrite and keep both files
		if (trackAlreadyDownloaded && this.settings.overwriteFile == OverwriteOption.KEEP_BOTH) {
			const baseFilename = `${filepath}/${filename}`
			let currentFilename
			let c = 0
			do {
				c++
				currentFilename = `${baseFilename} (${c})${extension}`
			} while (fs.existsSync(currentFilename))
			trackAlreadyDownloaded = false
			writepath = currentFilename
		}

		console.log('Downloading with bitrate: ', this.bitrate)

		var dlErr;

		// Download the track
		if (!trackAlreadyDownloaded || this.settings.overwriteFile == OverwriteOption.OVERWRITE) {
			const stream = fs.createWriteStream(writepath)
			try {
				if (this.bitrate < 0) {
					// Download with free-mp3-download
					let newExtension = '.' + await mp3dlSession.download(trackAPI_gw.SNG_ID, stream, track, this.downloadObject, this.listener, this.settings.fallbackBitrate, this.bitrate == -10 ? 'flac' : 'mp3')
					if (newExtension != extension) {
						let newWritepath = `${filepath}/${filename}${newExtension}`
						fs.renameSync(writepath,newWritepath)
						// Override previous
						extension = newExtension
						writepath = newWritepath
					}
				} else {
					// Download with deemix
					track.downloadURL = track.urls[formatsName[track.bitrate]]
					await streamTrack(stream, track, 0, this.downloadObject, this.listener)
				}
			} catch (e) {
				console.log("ERROR WHILE DOWNLOADING",e)
				fs.unlinkSync(writepath)
				// fs.rmSync(writepath)
				if (e instanceof got.HTTPError) dlErr = new DownloadFailed('notAvailable', track)
				dlErr = e
			}
			this.log(itemData, 'downloaded')
		} else {
			this.log(itemData, 'alreadyDownloaded')
			this.downloadObject.completeTrackProgress(this.listener)
		}

		// Throw if download went wrong
		if (dlErr) throw dlErr

		// Adding tags
		if (
			!trackAlreadyDownloaded ||
			([OverwriteOption.ONLY_TAGS, OverwriteOption.OVERWRITE].includes(this.settings.overwriteFile) && !track.local)
		) {
			this.log(itemData, 'tagging')
			if (extension == '.mp3') {
				tagID3(writepath, track, this.settings.tags)
				if (this.settings.tags.saveID3v1) tagID3v1(writepath, track, this.settings.tags)
			} else if (extension == '.flac') {
				tagFLAC(writepath, track, this.settings.tags)
			}
			this.log(itemData, 'tagged')
		}

		if (track.searched) returnData.searched = true
		this.downloadObject.downloaded += 1
		this.downloadObject.files.push(String(writepath))
		if (this.listener)
			this.listener.send('updateQueue', {
				uuid: this.downloadObject.uuid,
				downloaded: true,
				downloadPath: String(writepath),
				extrasPath: String(this.downloadObject.extrasPath)
			})
		returnData.filename = writepath.slice(extrasPath.length + 1)
		returnData.data = itemData
		return returnData
	}
}
