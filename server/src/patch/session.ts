/* eslint-disable */
import { WriteStream } from 'fs'
import stream, { EventEmitter } from 'stream'
import { promisify } from 'util'
import { nanoid } from 'nanoid'
import got from 'got'
import { patcherEvents } from '../app'


const pipeline = promisify(stream.pipeline)

class ErrCouldNotCommunicate extends Error {
	constructor(message: string) {
		super(message)
		this.name = "FMP3_CouldNotCommunicate"
	}
}

class ErrFlacNotAvailable extends Error {
	constructor(message: string) {
		super(message)
		this.name = "FMP3_FlacNotAvailable"
	}
}

export class mp3dlSession {
	static sessionId?: string
	static hValue?: string // Gotten when the session id is

	static async download(
		trackId: string,
		writestream: WriteStream,
		track: any,
		downloadObject: any,
		listener: any,
		fallbackBitrate: boolean,
		codec: string
	): Promise<string> {
		console.log('will download ', trackId)

		var website;
		//var codec = 'flac'

		// Check if flac is available
		if (codec == 'flac') {
			try {
				website = (await got(`https://free-mp3-download.net/download.php?id=${trackId}`, {
					headers: {
						'Accept': 'text/html'
					}
				}))

			} catch (err) {
				throw new ErrCouldNotCommunicate("Could not connect to free-mp3-download.net")
			}
			// Check if website shows flac being disabled
			if (website.body.includes('id="flac" disabled')) {
				if (fallbackBitrate) codec = 'mp3'
				else throw new ErrFlacNotAvailable("FLAC not available from free-mp3-download. If you want a 320kbps MP3 instead, enable bitrate fallback.")
			}
		}

		if (!this.sessionId) {
			await this.refreshCaptcha(trackId)
		}

		const itemData = {
			id: track.id,
			title: track.title,
			artist: track.mainArtist.name
		}

		const ch = nanoid(22)
		const headers = {
			Referer: `https://free-mp3-download.net/download.php?id=${trackId}`,
			Host: 'free-mp3-download.net',
			'X-Requested-With': 'XMLHttpRequest',
			Cookie: `PHPSESSID=${this.sessionId}`
		}

		listener.send('downloadInfo', {
			uuid: downloadObject.uuid,
			data: itemData,
			state: 'downloading',
			alreadyStarted: false,
			value: 200
		})

		const uploadProgressInterval = setInterval(async () => {
			try {
				const response = await got(`https://free-mp3-download.net/tmp/${ch}.txt`, {
					headers,
					throwHttpErrors: false
				})

				if (response.statusCode != 200) return;

				const progress = parseInt(response.body) / 100
				if (downloadObject.__type__ === 'Single') {
					downloadObject.progressNext = progress * 50
					downloadObject.updateProgress(listener)
				}

				console.log('GOT UPLOAD PROGRESS: ', progress)
			} catch (err) {
				console.log('failed to get upload progress')
			}
		}, 500)

		const urlReq = await got('https://free-mp3-download.net/dl.php', {
			searchParams: {
				i: trackId,
				ch,
				f: codec,
				h: this.hValue
			},
			followRedirect: false,
			headers
		})
		console.log("URL REQ:", urlReq.headers, urlReq.body)

		clearInterval(uploadProgressInterval)

		if (urlReq.statusCode == 200 && urlReq.body === 'Missing captcha') {
			// Missing captcha
			console.log('Missing captcha')
			await this.refreshCaptcha(trackId)
			await this.download(trackId, writestream, track, downloadObject, listener, fallbackBitrate, codec)
		} else if (urlReq.statusCode == 302 && typeof urlReq.headers.location === 'string') {
			// h value not needed for requests anymore
			this.hValue = undefined

			// Download progress things
			let chunkLength = 0
			let complete = 0

			const req = got
				.stream('https://free-mp3-download.net/dl.php', {
					followRedirect: true,
					throwHttpErrors: true,
					retry: {
						limit: 3,
					},
					searchParams: {
						i: trackId,
						ch,
						f: codec
					},
					headers
				})
				.on('response', response => {
					complete = parseInt(response.headers['content-length']) * 2
					chunkLength = complete / 2
					listener.send('downloadInfo', {
						uuid: downloadObject.uuid,
						data: itemData,
						state: 'downloading',
						alreadyStarted: false,
						value: complete
					})
				})
				.on('data', chunk => {
					if (downloadObject.isCanceled) {
						req.destroy()
					}
					chunkLength += chunk.length

					if (downloadObject) {
						let chunkProgres
						if (downloadObject.__type__ === 'Single') {
							chunkProgres = (chunkLength / complete) * 100
							downloadObject.progressNext = chunkProgres
						} else {
							chunkProgres = (chunk.length / (complete / 2) / downloadObject.size) * 100
							downloadObject.progressNext += chunkProgres
						}
						downloadObject.updateProgress(listener)
					}
				})
			console.log(req.requestUrl)
			await pipeline(req, writestream)
			return codec;
		}

		throw new Error(`The request to free-mp3-download returned status code ${urlReq.statusCode}`)
	}

	static refreshCaptcha(trackId: string): Promise<void> {
		return new Promise((res, rej) => {
			patcherEvents.once('show-captcha-result', (sessid, hValue) => {
				console.log('new sessid:', sessid, '\nh: ', hValue)
				this.sessionId = sessid
				this.hValue = hValue
				res()
			})
			patcherEvents.once('show-captcha-err', err => {
				rej(err)
			})
			patcherEvents.emit('show-captcha', trackId, this.sessionId)
		})
	}
}
